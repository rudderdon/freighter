﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino;
using Rhino.Commands;

namespace ProvingGround.FreighterLaunchUtils
{
    /// <summary>
    /// Guid Generator http://guidgenerator.com
    /// </summary>
    [Guid("2707cbb0-9c9d-438b-9645-9362815473d0"), CommandStyle(Style.Hidden)]
    public class PgShowGrasshopperRCP : Command
    {
        public PgShowGrasshopperRCP()
        {

            // Rhino only creates one instance of each command class defined in a
            // plug-in, so it is safe to store a refence in a static property.
            Instance = this;

        }

        /// <summary>
        /// The only instance of this command.
        /// </summary>
        public static PgShowGrasshopperRCP Instance
        {

            get;
            private set;

        }

        /// <returns>
        /// The command name as it appears on the Rhino command line.
        /// </returns>
        public override string EnglishName
        {

            get { return "PgShowGrasshopperRCP"; }

        }

        /// <summary>
        /// Opens the grasshopper remote control panel
        /// </summary>
        protected override Result RunCommand(RhinoDoc doc, RunMode mode)
        {

            // if the layer dockable panel is open, the remote control panel
            // will be attached to it as a sibling. Otherwise, just opens it.
            if (Rhino.UI.Panels.IsPanelVisible(Rhino.UI.PanelIds.Layers))
            {
                Rhino.UI.Panels.OpenPanelAsSibling(new Guid("b45a29b1-4343-4035-989e-044e8580d9cf"), Rhino.UI.PanelIds.Layers);
            }
            else
            {
                Rhino.UI.Panels.OpenPanel(new Guid("b45a29b1-4343-4035-989e-044e8580d9cf"));
            }

            return Result.Success;

        }
    }
}
