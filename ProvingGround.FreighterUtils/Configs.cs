﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace ProvingGround.FreighterUtils
{

    [System.Xml.Serialization.XmlRootAttribute("DocConfig")]
    public class DocConfig
    {
        #region Public Serializable Members

        /// <summary>
        /// List of attributes associated with freight
        /// </summary>
        [System.Xml.Serialization.XmlArrayAttribute("Attributes")]
        [System.Xml.Serialization.XmlArrayItemAttribute("Attribute")]
        public List<DocAttribute> Attributes { get; set; }

        /// <summary>
        /// List of assigned bools for managing freight
        /// </summary>
        [System.Xml.Serialization.XmlArrayAttribute("Toggles")]
        [System.Xml.Serialization.XmlArrayItemAttribute("ToggleEntry")]
        public List<ToggleEntry> Toggles { get; set; }

        /// <summary>
        /// List of components that require attendant attachments
        /// </summary>
        [System.Xml.Serialization.XmlArrayAttribute("AttachmentComps")]
        [System.Xml.Serialization.XmlArrayItemAttribute("AttachmentComp")]
        public List<AttachmentComponent> AttachmentComps { get; set; }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public DocConfig() { }
        #endregion

        #region Xml Read/Write Functionality

        /// <summary>
        /// Serializes a config file to xml
        /// </summary>
        /// <param name="FilePath"></param>
        /// <param name="ConfigToWrite"></param>
        public static void WriteConfig(string FilePath, DocConfig ConfigToWrite)
        {

            System.Xml.Serialization.XmlSerializer m_serializer = new System.Xml.Serialization.XmlSerializer(typeof(DocConfig));
            using (TextWriter m_writer = new StreamWriter(FilePath))
            {
                m_serializer.Serialize(m_writer, ConfigToWrite);
            }

        }

        /// <summary>
        /// Deserializes a config file from xml
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static DocConfig ReadConfig(string FilePath)
        {

            DocConfig m_thisConfig;

            try
            {
                using (var m_xmlReader = new StreamReader(FilePath))
                {
                    System.Xml.Serialization.XmlSerializer deserializer = new System.Xml.Serialization.XmlSerializer(typeof(DocConfig));
                    m_thisConfig = (DocConfig)deserializer.Deserialize(m_xmlReader);
                }

                return m_thisConfig;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Unable to read config file: " + ex.Message);
                return null;
            }

        }

        #endregion

        #region Return Values

        /// <summary>
        /// Gets the true/false value for a specified ToggleEntry
        /// </summary>
        /// <param name="Toggle"></param>
        /// <returns></returns>
        public bool GetToggleValue(string Toggle)
        {
            ToggleEntry m_thisToggle = 
                Toggles.Where(t => t.Toggle == Toggle).First();

            if (m_thisToggle != null)
            {
                return m_thisToggle.Value;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the string value for a specified DocAttribute
        /// </summary>
        /// <param name="Attribute"></param>
        /// <returns></returns>
        public string GetAttributeValue(string Attribute)
        {
            DocAttribute m_thisAttribute = 
                Attributes.Where(a => a.Attribute == Attribute).First();

            if(m_thisAttribute != null)
            {
                return m_thisAttribute.Value;
            }
            else
            {
                return "";
            }
        }
        #endregion

    }

    #region Doc Config Support Classes

    /// <summary>
    /// Single toggle entry
    /// </summary>
    public class ToggleEntry
    {
        [System.Xml.Serialization.XmlElementAttribute("Toggle")]
        public string Toggle { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Value")]
        public bool Value { get; set; }

        public ToggleEntry() { }
    }

    /// <summary>
    /// Single attribute entry
    /// </summary>
    public class DocAttribute
    {
        [System.Xml.Serialization.XmlElementAttribute("Attribute")]
        public string Attribute { get; set; }

        [System.Xml.Serialization.XmlElementAttribute("Value")]
        public string Value { get; set; }

        public DocAttribute() { }
    }

    /// <summary>
    /// Class for tracking panels or filepaths from GH document that may merit copying
    /// </summary>
    public class AttachmentComponent
    {

        [System.Xml.Serialization.XmlElementAttribute("InstanceGuid")]
        public Guid InstanceGuid { get; set; }

        [System.Xml.Serialization.XmlArrayAttribute("FileNames")]
        [System.Xml.Serialization.XmlArrayItemAttribute("FileName")]
        public List<string> FileNames { get; set; }

        public AttachmentComponent() { }

    }

    #endregion

}
